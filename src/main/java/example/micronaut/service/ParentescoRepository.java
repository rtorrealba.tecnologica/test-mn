package example.micronaut.service;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.reactive.RxJavaCrudRepository;
import io.micronaut.transaction.annotation.ReadOnly;
import io.reactivex.Flowable;

@Repository
public abstract class ParentescoRepository implements RxJavaCrudRepository<Parentesco, Integer>{
	
	@Inject
	private EntityManager entityManager;

	
	@ReadOnly
	public Flowable<Parentesco> findAll(){
		String jqlString = "FROM Parentesco";
		TypedQuery<Parentesco> query = entityManager.createQuery(jqlString, Parentesco.class);;
		return Flowable.fromIterable(query.getResultList());
	}

}
