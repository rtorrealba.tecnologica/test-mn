package example.micronaut.service;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.reactivex.Flowable;

@Controller
@Secured(SecurityRule.IS_ANONYMOUS)
public class Servicio {
	
	private ParentescoRepository parentezcoRepository;
	
	public Servicio(ParentescoRepository parentezcoRepository) {
		this.parentezcoRepository = parentezcoRepository;
	}
	
	@Get
	public Flowable<Parentesco> findAll(){
		return parentezcoRepository.findAll();
	}

}
